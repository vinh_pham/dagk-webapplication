//Initialize

 


var app = angular.module('linkedInApp', []);

app.controller('content-controller', ['$scope','$http',
    function ($scope, $http) {
        window.sc = $scope;
    
        $http.get('https://api.myjson.com/bins/28nia').success(function (data) {
            
            $scope.MyAvatar = data.MyAvatar;
            $scope.MyFullName = data.MyFullName;
            $scope.MyHeadline = data.MyHeadline;
            $scope.Working = data.Working;           
            $scope.Summary = data.Summary;
            $scope.Experience = data.Experience;
            $scope.MySkills = data.MySkills;      
            $scope.MyEducation = data.MyEducation;
            
        });
    }]);
	
app.directive("editYourName", function () {
    var editorTemplate = '' +
        '<div class="edit-Your-Name">' +
            '<div ng-hide="view.editorEnabled">' +
                '{{value}} ' +
                '<a class="button tiny" ng-click="enableEditor()">Edit</a>' +
            '</div>' +
            '<div ng-show="view.editorEnabled">' +
                '<input type="text" class="small-12.columns" ng-model="view.editableValue">' +
                '<a class="button tiny" href="#" ng-click="save()">Save</a>' +
                ' or ' +
                '<a class="button tiny" href="#" ng-click="disableEditor()">Cancel</a>' +
            '</div>' +
        '</div>';

    return {
        restrict: "A",
        replace: true,
        template: editorTemplate,
        scope: {
            value: "=editYourName",
        },
        link: function (scope, element, attrs) {
            scope.view = {
                editableValue: scope.value,
                editorEnabled: false
            };

            scope.enableEditor = function () {
                scope.view.editorEnabled = true;
                scope.view.editableValue = scope.value;
                setTimeout(function () {
                    element.find('input')[0].focus();
                    //element.find('input').focus().select(); // w/ jQuery
                });
            };

            scope.disableEditor = function () {
                scope.view.editorEnabled = false;
            };

            scope.save = function () {
                scope.value = scope.view.editableValue;
                scope.disableEditor();
            };

        }
    };
});
app.directive("editMyHeadline", function () {
		var editorTemplate = '' +
			'<div class="edit-My-Headline">' +
				'<div ng-hide="view.editorEnabled">' +
					'{{value}} ' +
					'<a class="button tiny" ng-click="enableEditor()">Edit</a>' +
				'</div>' +
				'<div ng-show="view.editorEnabled">' +
					'<input type="text" class="small-12.columns" ng-model="view.editableValue">' +
					'<a class="button tiny" href="#" ng-click="save()">Save</a>' +
					' or ' +
					'<a class="button tiny" href="#" ng-click="disableEditor()">Cancel</a>' +
            	'</div>' +
			'</div>';
	
		return {
			restrict: "A",
			replace: true,
			template: editorTemplate,
			scope: {
				value: "=editMyHeadline",
			},
			link: function (scope, element, attrs) {
				scope.view = {
					editableValue: scope.value,
					editorEnabled: false
				};
	
				scope.enableEditor = function () {
					scope.view.editorEnabled = true;
					scope.view.editableValue = scope.value;
					setTimeout(function () {
						element.find('input')[0].focus();
						//element.find('input').focus().select(); // w/ jQuery
					});
				};
	
				scope.disableEditor = function () {
					scope.view.editorEnabled = false;
				};
	
				scope.save = function () {
					scope.value = scope.view.editableValue;
					scope.disableEditor();
				};
	
			}
		};
	});
app.directive("editWorking", function () {
		var editorTemplate = '' +
			'<div class="edit-Working">' +
				'<div ng-hide="view.editorEnabled">' +
					'{{value}} ' +
					'<a class="button tiny" ng-click="enableEditor()">Edit</a>' +
				'</div>' +
				'<div ng-show="view.editorEnabled">' +
					'<input type="text" class="small-12.columns" ng-model="view.editableValue">' +
					'<a class="button tiny" href="#" ng-click="save()">Save</a>' +
					' or ' +
					'<a class="button tiny" href="#" ng-click="disableEditor()">Cancel</a>' +
            	'</div>' +
			'</div>';
	
		return {
			restrict: "A",
			replace: true,
			template: editorTemplate,
			scope: {
				value: "=editWorking",
			},
			link: function (scope, element, attrs) {
				scope.view = {
					editableValue: scope.value,
					editorEnabled: false
				};
	
				scope.enableEditor = function () {
					scope.view.editorEnabled = true;
					scope.view.editableValue = scope.value;
					setTimeout(function () {
						element.find('input')[0].focus();
						//element.find('input').focus().select(); // w/ jQuery
					});
				};
	
				scope.disableEditor = function () {
					scope.view.editorEnabled = false;
				};
	
				scope.save = function () {
					scope.value = scope.view.editableValue;
					scope.disableEditor();
				};
	
			}
		};
	});
app.directive("editCurrent", function () {
		var editorTemplate = '' +
			'<div class="edit-Current">' +
				'<div ng-hide="view.editorEnabled">' +
					'{{value}} ' +
					'<a class="button tiny" ng-click="enableEditor()">Edit</a>' +
				'</div>' +
				'<div ng-show="view.editorEnabled">' +
					'<input type="text" class="small-12.columns" ng-model="view.editableValue">' +
					'<a class="button tiny" href="#" ng-click="save()">Save</a>' +
					' or ' +
					'<a class="button tiny" href="#" ng-click="disableEditor()">Cancel</a>' +
            	'</div>' +
			'</div>';
	
		return {
			restrict: "A",
			replace: true,
			template: editorTemplate,
			scope: {
				value: "=editCurrent",
			},
			link: function (scope, element, attrs) {
				scope.view = {
					editableValue: scope.value,
					editorEnabled: false
				};
	
				scope.enableEditor = function () {
					scope.view.editorEnabled = true;
					scope.view.editableValue = scope.value;
					setTimeout(function () {
						element.find('input')[0].focus();
					});
				};
	
				scope.disableEditor = function () {
					scope.view.editorEnabled = false;
				};
	
				scope.save = function () {
					scope.value = scope.view.editableValue;
					scope.disableEditor();
				};
	
			}
		};
	});
app.directive("editTittle", function () {
		var editorTemplate = '' +
			'<div class="edit-Tittle">' +
				'<div ng-hide="view.editorEnabled">' +
					'{{value}} ' +
					'<a class="button tiny" ng-click="enableEditor()">Edit</a>' +
				'</div>' +
				'<div ng-show="view.editorEnabled">' +
					'<input type="text" class="small-12.columns" ng-model="view.editableValue">' +
					'<a class="button tiny" href="#" ng-click="save()">Save</a>' +
					' or ' +
					'<a class="button tiny" href="#" ng-click="disableEditor()">Cancel</a>' +
            	'</div>' +
			'</div>';
	
		return {
			restrict: "A",
			replace: true,
			template: editorTemplate,
			scope: {
				value: "=editTittle",
			},
			link: function (scope, element, attrs) {
				scope.view = {
					editableValue: scope.value,
					editorEnabled: false
				};
	
				scope.enableEditor = function () {
					scope.view.editorEnabled = true;
					scope.view.editableValue = scope.value;
					setTimeout(function () {
						element.find('input')[0].focus();
					});
				};
	
				scope.disableEditor = function () {
					scope.view.editorEnabled = false;
				};
	
				scope.save = function () {
					scope.value = scope.view.editableValue;
					scope.disableEditor();
				};
	
			}
		};
	});
app.directive("editTittleCty", function () {
		var editorTemplate = '' +
			'<div class="edit-Tittle_Cty">' +
				'<div ng-hide="view.editorEnabled">' +
					'{{value}} ' +
					'<a class="button tiny" ng-click="enableEditor()">Edit</a>' +
				'</div>' +
				'<div ng-show="view.editorEnabled">' +
					'<input type="text" class="small-12.columns" ng-model="view.editableValue">' +
					'<a class="button tiny" href="#" ng-click="save()">Save</a>' +
					' or ' +
					'<a class="button tiny" href="#" ng-click="disableEditor()">Cancel</a>' +
            	'</div>' +
			'</div>';
	
		return {
			restrict: "A",
			replace: true,
			template: editorTemplate,
			scope: {
				value: "=editTittleCty",
			},
			link: function (scope, element, attrs) {
				scope.view = {
					editableValue: scope.value,
					editorEnabled: false
				};
	
				scope.enableEditor = function () {
					scope.view.editorEnabled = true;
					scope.view.editableValue = scope.value;
					setTimeout(function () {
						element.find('input')[0].focus();
					});
				};
	
				scope.disableEditor = function () {
					scope.view.editorEnabled = false;
				};
	
				scope.save = function () {
					scope.value = scope.view.editableValue;
					scope.disableEditor();
				};
	
			}
		};
	});
app.directive("editSchool", function () {
		var editorTemplate = '' +
			'<div class="edit-School">' +
				'<div ng-hide="view.editorEnabled">' +
					'{{value}} ' +
					'<a class="button tiny" ng-click="enableEditor()">Edit</a>' +
				'</div>' +
				'<div ng-show="view.editorEnabled">' +
					'<input type="text" class="small-12.columns" ng-model="view.editableValue">' +
					'<a class="button tiny" href="#" ng-click="save()">Save</a>' +
					' or ' +
					'<a class="button tiny" href="#" ng-click="disableEditor()">Cancel</a>' +
            	'</div>' +
			'</div>';
	
		return {
			restrict: "A",
			replace: true,
			template: editorTemplate,
			scope: {
				value: "=editSchool",
			},
			link: function (scope, element, attrs) {
				scope.view = {
					editableValue: scope.value,
					editorEnabled: false
				};
	
				scope.enableEditor = function () {
					scope.view.editorEnabled = true;
					scope.view.editableValue = scope.value;
					setTimeout(function () {
						element.find('input')[0].focus();
					});
				};
	
				scope.disableEditor = function () {
					scope.view.editorEnabled = false;
				};
	
				scope.save = function () {
					scope.value = scope.view.editableValue;
					scope.disableEditor();
				};
	
			}
		};
	});
app.directive("editDate", function () {
		var editorTemplate = '' +
			'<div class="edit-Date">' +
				'<div ng-hide="view.editorEnabled">' +
					'{{value}} ' +
					'<a class="button tiny" ng-click="enableEditor()">Edit</a>' +
				'</div>' +
				'<div ng-show="view.editorEnabled">' +
					'<input type="text" class="small-12.columns" ng-model="view.editableValue">' +
					'<a class="button tiny" href="#" ng-click="save()">Save</a>' +
					' or ' +
					'<a class="button tiny" href="#" ng-click="disableEditor()">Cancel</a>' +
            	'</div>' +
			'</div>';
	
		return {
			restrict: "A",
			replace: true,
			template: editorTemplate,
			scope: {
				value: "=editDate",
			},
			link: function (scope, element, attrs) {
				scope.view = {
					editableValue: scope.value,
					editorEnabled: false
				};
	
				scope.enableEditor = function () {
					scope.view.editorEnabled = true;
					scope.view.editableValue = scope.value;
					setTimeout(function () {
						element.find('input')[0].focus();
					});
				};
	
				scope.disableEditor = function () {
					scope.view.editorEnabled = false;
				};
	
				scope.save = function () {
					scope.value = scope.view.editableValue;
					scope.disableEditor();
				};
	
			}
		};
	});

app.directive("clickToEdit", function () {
		var editorTemplate = '' +
			'<div class="click-to-edit">' +
				'<div ng-hide="view.editorEnabled">' +
					'{{value}} ' +
					'<a class="button tiny" ng-click="enableEditor()">Edit</a>' +
				'</div>' +
				'<div ng-show="view.editorEnabled">' +
					'<textarea rows="5" cols="100" class="small-12.columns" ng-model="view.editableValue"/>' +
					
					
					'<a class="button tiny" href="#" ng-click="save()">Save</a>' +
					
					' or ' +
					
					'<a class="button tiny" href="#" ng-click="disableEditor()">Cancel</a>' +
				'</div>' +
			'</div>';
	
		return {
			restrict: "A",
			replace: true,
			template: editorTemplate,
			scope: {
				value: "=clickToEdit",
			},
			link: function (scope, element, attrs) {
				scope.view = {
					editableValue: scope.value,
					editorEnabled: false
				};
	
				scope.enableEditor = function () {
					scope.view.editorEnabled = true;
					scope.view.editableValue = scope.value;
					setTimeout(function () {
						element.find('input')[0].focus();
						//element.find('input').focus().select(); // w/ jQuery
					});
				};
	
				scope.disableEditor = function () {
					scope.view.editorEnabled = false;
				};
	
				scope.save = function () {
					scope.value = scope.view.editableValue;
					scope.disableEditor();
				};
	
			}
		};
	});
